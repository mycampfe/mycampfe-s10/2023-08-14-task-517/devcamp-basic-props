import CountClick from "./components/CountClick";
import Info from "./components/Info";

function App() {
  return (
    <div style={{margin: "50px"}}>
      <div>
        <p>517.10 Props</p>

        <Info firstName="Minh" lastName="Vu Dang" favNumber={25}>This is a children</Info>
      </div>

      <div>
        <p>517.20 State</p>

        <CountClick />
      </div>
    </div>
  );
}

export default App;
