import { Component } from "react";

class CountClick extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        }

        // this.onBtnCountClick = this.onBtnCountClick.bind(this);
    }

    onBtnCountClick = () => {
        console.log("CLICKED");

        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <div>
                <div>
                    <button onClick={this.onBtnCountClick}>Click me</button>
                </div>
                <div>
                    <p>You click {this.state.count} times</p>
                </div>
            </div>
        )   
    }
}

export default CountClick;